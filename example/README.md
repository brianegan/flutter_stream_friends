# flutter_stream_friends_example

A demonstration of the `StreamWidget` and `StreamCallback` classes found within `flutter_stream_friends_example`. For full docs, please see the parent project!
 
## Running the Project 

  * Install Flutter + dependencies! For help getting started with Flutter, view their online
[documentation](http://flutter.io/).
  * Open an iOS Simulator or Android Emulator
  * Clone this repo
  * Run `flutter packages get`
  * Then run `flutter run --debug`
